import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_auc_score

df = pd.read_csv('heart.csv')
X = df.iloc[:,:-1]
y = df['target']
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=42)
clf = RandomForestClassifier(n_estimators=700,
                             max_depth=4,
                             random_state=0)
clf.fit(X_train, y_train)
pred = clf.predict(X_test)
acc = roc_auc_score(y_test, pred)
print('Accuracy:', acc)

from joblib import dump
dump(clf, 'classifier.joblib')
