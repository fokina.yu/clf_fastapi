import uvicorn
from fastapi import FastAPI
from pydantic import BaseModel
import joblib

class People(BaseModel):
    age: float
    sex: int
    cp: float
    trestbps: float
    chol: float
    fbs: float
    restecg: float
    thalach: float
    exang: float
    oldpeak: float
    slope: float
    ca: float
    thal: float


app = FastAPI()
model = joblib.load('classifier.joblib')

@app.get('/')
def index():
    return {'message': 'This is the homepage of the API '}

@app.post('/prediction')
def get_clf(data: People):
    received = data.dict()
    age = received['age']
    sex = received['sex']
    cp = received['cp']
    trestbps = received['trestbps']
    chol = received['chol']
    fbs = received['fbs']
    restecg = received['restecg']
    thalach = received['thalach']

    exang = received['exang']
    oldpeak = received['oldpeak']
    slope = received['slope']
    ca = received['ca']
    thal = received['thal']

    pred_name = model.predict([[age,sex,cp,trestbps,chol,fbs,restecg,thalach,exang,oldpeak,slope,ca,thal]]).tolist()[0]

    return {'prediction': pred_name}

if __name__ == '__main__':

    uvicorn.run(app, host='0.0.0.0', port=4000, debug=True)
