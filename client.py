import requests
import json

url = "http://0.0.0.0:4000/prediction"

payload = json.dumps({
    'age':60,'sex':1,'cp':3,'trestbps':145,'chol':233,'fbs':1,'restecg':0,'thalach':150,'exang':0,'oldpeak':2,'slope':3,'ca':0,'thal':0})

headers = {
  'Content-Type': 'application/json'
}

response = requests.request("POST", url, headers=headers, data=payload)

print(response.text)

