scikit-learn==0.24.2
fastapi==0.67.0
pandas==1.3.0
requests==2.26.0
uvicorn==0.14.0
joblib==1.0.1
pydantic==1.8.2
