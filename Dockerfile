FROM python:3
WORKDIR /app
COPY ./requirements.txt requirements.txt
RUN pip3 install --no-cache-dir -r requirements.txt
COPY . ./
ENTRYPOINT ["python3"]
CMD ["heart_disease_2.py"]
